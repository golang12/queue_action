package queue

import (
	"sync"
	"sync/atomic"
)

var queue = make(map[int]*Item)
var counter int32
var mu = sync.Mutex{}

type Action interface {
	Do()
}

type Item struct {
	Ch    chan Action
	Count int
	Mu    *sync.Mutex
}

func New(Key int, act Action) {
	q := Item{make(chan Action, 1), 1, &sync.Mutex{}}
	q.Ch <- act
	queue[Key] = &q
	go q.Do(Key)
}

//Удаление очереди,общая блокировка для безопасности карты
func (q *Item) Delete(Key int) {
	mu.Lock()
	delete(queue, Key)
	mu.Unlock()
}

//Добавлеие в очередь
func (q *Item) Inc(Key int, act Action) {
	queue[Key].Count++
	queue[Key].Ch <- act
}

func (q *Item) Dec(Key int) {
	q.Count--
	if q.Count <= 0 {
		go q.Delete(Key)
	}
}

//общая блокировка для безопасности карты
func Add(Key int, act Action) {
	mu.Lock()
	if val, ok := queue[Key]; ok {
		val.Inc(Key, act) //Добавление элемента в очередь
	} else {
		New(Key, act) //Новая очередь
	}
	mu.Unlock()
}

func (q *Item) Do(Key int) {
	q.Mu.Lock()
	defer func() { atomic.AddInt32(&counter, -1) }()
	for {
		action, ok := <-q.Ch
		if ok {
			action.Do()
			q.Dec(Key)
		} else {
			break
		}
	}
	q.Mu.Unlock()
}

